package com.magesho.wastemanager;

public interface ApiResponseCallback {
    public void success(Response response);

    public void fail();
}
