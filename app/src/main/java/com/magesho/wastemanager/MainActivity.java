package com.magesho.wastemanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//buttons to go to registration and login pages for users and admin
        // button for admin login
        Button adminLogin = (Button) findViewById(R.id.button);
        adminLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), Main2Activity.class);
                myIntent.putExtra(Main2Activity.KEY_LOG_IN_AS, "admin");
                startActivity(myIntent);


        }});

        //button for user login
        Button userLogin = (Button) findViewById(R.id.button2);
        userLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), Main2Activity.class);
                myIntent.putExtra(Main2Activity.KEY_LOG_IN_AS, "user");
                startActivity(myIntent);
            }
        });
    }
}
