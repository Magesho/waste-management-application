package com.magesho.wastemanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity implements View.OnClickListener{

    public static final String KEY_LOG_IN_AS = "key_log_in_as";

    private Button buttonLogin;
    private EditText editText;
    private EditText editText2;
    private ProgressDialog progressDialog;
    private ApiAdapter apiAdapter;
    private boolean userLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Bundle bundle = getIntent().getExtras();
        userLogin = false;
        if(bundle != null){
            String loggedInAs = bundle.getString(KEY_LOG_IN_AS, "user");
            if(loggedInAs.equals("admin")) userLogin = false;
            else if(loggedInAs.equals("user")) userLogin = true;
        }

        apiAdapter = new ApiAdapter();

        buttonLogin=(Button) findViewById(R.id.buttonLogin);
        editText=(EditText) findViewById(R.id.editText);
        editText2=(EditText) findViewById(R.id.editText2);
        buttonLogin.setOnClickListener(this);
        progressDialog= new ProgressDialog(this);



        Button button6 = (Button) findViewById(R.id.buttonRegister);
        button6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), Main3Activity.class);
                startActivity(myIntent);

            }
        });

    }




    private void loginUser() {

        final String email=editText.getText().toString().trim();
        String password=editText2.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            //if email is empty
            Toast.makeText(this,"Please enter your email address",Toast.LENGTH_SHORT).show();
            //stopping function executing further
            return;
    }
        if(TextUtils.isEmpty(password)){
            //if email is empty
            Toast.makeText(this,"Please enter your password",Toast.LENGTH_SHORT).show();
            //stopping function executing further
            return;
        }
        //if validation is okay
        progressDialog.setMessage("Loging in please wait..");
        progressDialog.show();

        //send to api to login user
        ApiResponseCallback callback = new ApiResponseCallback() {
            @Override
            public void success(Response response) {
                progressDialog.hide();
                if(response.getMsg().equals("Success")){
                            Intent myIntent = new Intent(Main2Activity.this, UserRequestActivity.class);
                            startActivity(myIntent);
                    Toast.makeText(Main2Activity.this, "Login successful",Toast.LENGTH_SHORT).show();
                    //moves to the requests page

                }
                else{
                    Toast.makeText(Main2Activity.this, "Login Failed",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void fail() {
                progressDialog.hide();
                Toast.makeText(Main2Activity.this, "Login failed",Toast.LENGTH_SHORT).show();
            }
        };
        apiAdapter.adminlogin(email, password, callback);
}



    private void loginAdmin() {

        final String email=editText.getText().toString().trim();
        String password=editText2.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            //if email is empty
            Toast.makeText(this,"Please enter your email address",Toast.LENGTH_SHORT).show();
            //stopping function executing further
            return;
        }
        if(TextUtils.isEmpty(password)){
            //if email is empty
            Toast.makeText(this,"Please enter your password",Toast.LENGTH_SHORT).show();
            //stopping function executing further
            return;
        }
        //if validation is okay
        progressDialog.setMessage("Loging in please wait..");
        progressDialog.show();

        //send to api to login user
        ApiResponseCallback callback = new ApiResponseCallback() {
            @Override
            public void success(Response response) {
                progressDialog.hide();
                if(response.getMsg().equals("Success")){
                    Intent myIntent = new Intent(Main2Activity.this, AdminActivity.class);
                    startActivity(myIntent);
                    Toast.makeText(Main2Activity.this, "Login successful",Toast.LENGTH_SHORT).show();
                    //moves to the requests page

                }
                else{
                    Toast.makeText(Main2Activity.this, "Login Failed",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void fail() {
                progressDialog.hide();
                Toast.makeText(Main2Activity.this, "Login failed",Toast.LENGTH_SHORT).show();
            }
        };
        apiAdapter.userLogin(email, password, callback);
    }
    @Override
    public void onClick(View v) {
        if (v == buttonLogin) {
            if(userLogin)loginUser();
            else loginAdmin();
        }

    }
}


