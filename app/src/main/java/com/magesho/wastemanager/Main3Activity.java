package com.magesho.wastemanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class Main3Activity extends AppCompatActivity implements View.OnClickListener{

    private EditText editTextEmail;
    private EditText editTextPassword;
    private Button buttonSubmit;
    private ProgressDialog progressDialog;
    private ApiAdapter apiAdapter;

    private FirebaseAuth firebaseAuth;  //for firebase ....

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        apiAdapter = new ApiAdapter();
        progressDialog= new ProgressDialog(this);
        //initialises firebase auth object
        firebaseAuth= FirebaseAuth.getInstance();
        //initialization
        buttonSubmit=(Button) findViewById(R.id.buttonSubmit);
        editTextEmail=(EditText) findViewById(R.id.editTextEmail);
        editTextPassword=(EditText) findViewById(R.id.editTextPassword);

        buttonSubmit.setOnClickListener(this);

    }

        //user registration method
    private void registerUser(){
        String email=editTextEmail.getText().toString().trim();
        String password=editTextPassword.getText().toString().trim();


        if(TextUtils.isEmpty(email)){
            //if email is empty
            Toast.makeText(this,"Please enter your email address",Toast.LENGTH_SHORT).show();
            //stopping function executing further
            return;
        }
        if(TextUtils.isEmpty(password)){
            //if email is empty
            Toast.makeText(this,"Please enter your password",Toast.LENGTH_SHORT).show();
            //stopping function executing further
            return;
        }
        //if validations are ok
        //progress dialog shown
        progressDialog.setMessage("Registering please wait..");
        progressDialog.show();


        //Send to the api to register
        ApiResponseCallback callback = new ApiResponseCallback() {
            @Override
            public void success(Response response) {
                progressDialog.hide();
                if(response.getMsg().equals("Success")){
                    Toast.makeText(Main3Activity.this, "Registration successful",Toast.LENGTH_SHORT).show();
                    //moves to the requests page
                    Intent myIntent = new Intent(Main3Activity.this, UserRequestActivity.class);
                    startActivity(myIntent);
                }
                else{
                    Toast.makeText(Main3Activity.this, "Registration Failed",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void fail() {
                progressDialog.hide();
                Toast.makeText(Main3Activity.this, "Registration failed",Toast.LENGTH_SHORT).show();
            }
        };
        apiAdapter.userRegister(email, password, callback);


        //firebaseAuth.createUserWithEmailAndPassword(email,password)
//                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                    if(task.isSuccessful()){
//                        Toast.makeText(Main3Activity.this, "Registration successful",Toast.LENGTH_SHORT).show();
//                    }else {
//                        Toast.makeText(Main3Activity.this, "Registration failed",Toast.LENGTH_SHORT).show();
//                    }
//                    }
//                });

    }
    @Override
    public void onClick(View v) {
        if (v == buttonSubmit){
            registerUser();
        }

    }
}
