package com.magesho.wastemanager;

import android.util.Log;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
    //holds the register, login ,requests methods
public class ApiAdapter {

    UserApiClient userApiClient;

    public ApiAdapter(){
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://192.168.43.47/")  //url for database in localhost
                .addConverterFactory(GsonConverterFactory.create());

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder okBuilder = new OkHttpClient.Builder();
        okBuilder.addInterceptor(interceptor);

        Retrofit retrofit = builder.client(okBuilder.build()).build();

        userApiClient = retrofit.create(UserApiClient.class);
    }
    //login method
    public void userLogin(String user_email, String user_password, final ApiResponseCallback callback){

        Call<Response> call = userApiClient.userLogin(user_email, user_password);

        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if(response.isSuccessful()){
                    Log.i("ApiAdapter", "call url:" + call.request().url().toString()+"Success");
                    callback.success(response.body());


                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Log.i("ApiAdapter", "call url:" + call.request().url().toString()+"Failed");
                callback.fail();


            }
        });
    }
    //user register method
    public void userRegister(String user_email, String user_password, final ApiResponseCallback callback){

        Call<Response> call = userApiClient.userRegister(user_email, user_password);

        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if(response.isSuccessful()){
                    Log.i("ApiAdapter", "call url: " + call.request().url().toString() + " Success");
                    callback.success(response.body());
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Log.i("ApiAdapter", "call url: " + call.request().url().toString() + " Failed");
                callback.fail();
            }
        });
    }

    //user requests method
    public void userRequests(String name, String Quantity,boolean Paper,boolean Metal, boolean Plastic, boolean Wood,boolean Foodmaterial, final ApiResponseCallback callback){

        Call<Response> call = userApiClient.userRequests(name,  Quantity,Paper, Metal, Plastic, Wood, Foodmaterial);

        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if(response.isSuccessful()){
                    Log.i("ApiAdapter", "call url: " + call.request().url().toString() + " Success");
                    callback.success(response.body());
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Log.i("ApiAdapter", "call url: " + call.request().url().toString() + " Failed");
                callback.fail();
            }
        });
    }
    //admin login method
    public void adminlogin(String user_email, String user_password, final ApiResponseCallback callback){

        Call<Response> call = userApiClient.userLogin(user_email, user_password);

        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if(response.isSuccessful()){
                    Log.i("ApiAdapter", "call url:" + call.request().url().toString()+"Success");
                    callback.success(response.body());


                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Log.i("ApiAdapter", "call url:" + call.request().url().toString()+"Failed");
                callback.fail();


            }
        });
    }




}
