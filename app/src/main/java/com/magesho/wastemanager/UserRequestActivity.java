package com.magesho.wastemanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class UserRequestActivity extends AppCompatActivity implements View.OnClickListener {

    private CheckBox checkBoxPaper;
    private CheckBox checkBoxWood;
    private CheckBox checkBoxMetal;
    private CheckBox checkBoxPlastic;
    private CheckBox checkBoxFoodMaterial;
    private EditText editTextQuantity;
    private EditText editTextName;
    private ApiAdapter apiAdapter;
    private Button button3SubmitRequest;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_request);
        apiAdapter = new ApiAdapter();

        //checkboxes
        checkBoxPaper = (CheckBox) findViewById(R.id.checkBoxPaper);
        checkBoxWood = (CheckBox) findViewById(R.id.checkBoxWood);
        checkBoxMetal = (CheckBox) findViewById(R.id.checkBoxMetal);
        checkBoxPlastic = (CheckBox) findViewById(R.id.checkBoxPlastic);
        checkBoxFoodMaterial = (CheckBox) findViewById(R.id.checkBoxFoodMaterial);
        button3SubmitRequest = (Button) findViewById(R.id.button3SubmitRequest);
        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextQuantity = (EditText) findViewById(R.id.editTextQuantity);
        button3SubmitRequest.setOnClickListener(this);
        progressDialog = new ProgressDialog(this);

        //used for testing the checkboxes
        checkBoxPaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("WASTE", "onClick: checkBoxPaper: " + checkBoxPaper.isChecked());
            }
        });


    }
        //user requests method
    private void userRequests() {

        String name=editTextName.getText().toString().trim();
        String Quantity=editTextQuantity.getText().toString().trim();
        String Paper= checkBoxPaper.getText().toString().trim();
        String Wood= checkBoxWood.getText().toString().trim();
        String Metal= checkBoxMetal.getText().toString().trim();
        String Plastic= checkBoxPlastic.getText().toString().trim();
        String FoodMaterial= checkBoxFoodMaterial.getText().toString().trim();
        //the checkboxes if checked
        boolean paper = checkBoxPaper.isChecked();
        boolean wood = checkBoxWood.isChecked();
        boolean metal = checkBoxMetal.isChecked();
        boolean plastic = checkBoxPlastic.isChecked();
        boolean foodMaterial = checkBoxFoodMaterial.isChecked();

        //checks for the different fields if filled
        if(TextUtils.isEmpty(name)){
            //if email is empty
            Toast.makeText(this,"Please enter your name",Toast.LENGTH_SHORT).show();
            //stopping function executing further
            return;
        }
        if(TextUtils.isEmpty(Quantity)){
            //if email is empty
            Toast.makeText(this,"Please enter the amount",Toast.LENGTH_SHORT).show();
            //stopping function executing further
            return;
        }
        //if validation is okay
        progressDialog.setMessage("Submiting  please wait..");
        progressDialog.show();

        //send to api to login user
        ApiResponseCallback callback = new ApiResponseCallback() {
            @Override
            public void success(Response response) {
                progressDialog.hide();
                if(response.getMsg().equals("Success")){
                    Toast.makeText(UserRequestActivity.this, "Request submitted successful",Toast.LENGTH_SHORT).show();
                    //moves to the requests page
                    Intent myIntent = new Intent(UserRequestActivity.this, UserRequestActivity.class);
                    startActivity(myIntent);
                }
                else{
                    Toast.makeText(UserRequestActivity.this, "Submission unsuccesfull",Toast.LENGTH_SHORT).show();
                }
            }

           @Override
           public void fail() {
               progressDialog.hide();
               Toast.makeText(UserRequestActivity.this, "Submission failed",Toast.LENGTH_SHORT).show();
            }
        };
        apiAdapter.userRequests(name, Quantity, paper,wood,metal, plastic, foodMaterial, callback);
    }



    @Override
    public void onClick(View v) {
        if (v == button3SubmitRequest) userRequests();

    }
    //logout menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menuLogout:
                //SharedPrefManager.getInstance(this).logout;
                startActivity(new Intent(this, Main2Activity.class));
                Toast.makeText(UserRequestActivity.this, "Logged Out",Toast.LENGTH_SHORT).show();
                UserRequestActivity.this.finish();
                break;
        }
        return true;
    }
}
