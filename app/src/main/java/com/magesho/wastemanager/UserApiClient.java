package com.magesho.wastemanager;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface UserApiClient {
        //login php script
    @FormUrlEncoded
    @POST("waste/userlogin.php")
    Call<Response>userLogin(
          @Field("user_email") String user_email,
          @Field("user_password") String user_password
    );

        //register php script
    @FormUrlEncoded
    @POST("waste/userregister.php")
    Call<Response>userRegister(
            @Field("user_email") String user_email,
            @Field("user_password") String user_password
    );

    //requests php script
    @FormUrlEncoded
    @POST("waste/requests.php")
    Call<Response>userRequests(
            @Field("Name") String name,
            @Field("quantity") String Quantity,
            @Field("paper") boolean Paper,
            @Field("metal") boolean Metal,
            @Field("plastic") boolean Plastic,
            @Field("wood") boolean Wood,
            @Field("food_waste") boolean FoodMaterial
    );
    //admin login php script
    @FormUrlEncoded
    @POST("waste/adminlogin.php")
    Call<Response>adminlogin(
            @Field("user_email") String user_email,
            @Field("user_password") String user_password
    );


}
